#include <stdio.h>
#include <string.h>

#include "myassert.h"

#include "manip.h"

void repeter(const char *s, int n)
{
    myassert(n >= 0, "le nombre de répétition doit être positif ou nul");

    for (int i = 0; i < n; i++)
        printf("%s", s);
}

void encadrer(const char *s)
{
    int l = strlen(s);

    printf("+");
    repeter("-", l+2);
    printf("+\n");

    printf("| %s |\n", s);

    printf("+");
    repeter("-", l+2);
    printf("+\n");
}

void souligner(const char *s)
{
    int l = strlen(s);
    printf("%s\n", s);
    repeter("-", l);
    printf("\n");
}

void surligner(const char *s)
{
    int l = strlen(s);
    repeter("-", l);
    printf("\n");
    printf("%s\n", s);
}
