#include <stdio.h>
#include <stdlib.h>

#include "manip.h"

static void usage(const char *exeName, const char *message)
{
    fprintf(stderr, "usage : %s <n>\n", exeName);
    fprintf(stderr, "   <n> : nombre de répétitions\n");
    if (message != NULL)
        fprintf(stderr, "message : %s\n", message);
    exit(EXIT_FAILURE);
}


int main(int argc, char * argv[])
{
    if (argc != 2)
        usage(argv[0], "nombre d'arguments incorrect");

    int n = atoi(argv[1]);
    repeter("Hello world!\n", n);
    
    encadrer("Hello world!");
    souligner("Hello world!");
    surligner("Hello world!");

    return EXIT_SUCCESS;
}
