#ifndef MANIP_H
#define MANIP_H

void repeter(const char *s, int n);
void encadrer(const char *s);
void souligner(const char *s);
void surligner(const char *s);

#endif
