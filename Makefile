
SHELL = /bin/sh

INSTALL = cp
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA    = $(INSTALL)

#########################################################
# Compiler and options
#########################################################

CC = gcc
CPP = gcc -E

INCDIR = -I.

CPPFLAGS = $(INCDIR)

CFLAGS = -g -Wall -Wextra -pedantic -std=c99
#CFLAGS = -O -Wall


#LIBS = -ljpeg -lXaw -lm
LIBS = -lm

#LDFLAGS = -L/usr/local/X11R6/lib -L../jpeg $(LIBS)
LDFLAGS = $(LIBS)



#########################################################
# Common prefix for installation directories.
#########################################################

#prefix = /usr/local
prefix = .
exec_prefix = ${prefix}

bindir  = ${exec_prefix}/bin
sbindir = ${exec_prefix}/sbin
libexecdir = ${exec_prefix}/libexec

libdir = ${exec_prefix}/lib
infodir = ${prefix}/info
includedir = ${prefix}/include
mandir = ${prefix}/man

srcdir  = .


#########################################################
# bin, sources, ...
#########################################################

BIN = hello

SRC = hello.c \
      manip.c \
      myassert.c

OBJ = $(subst .c,.o,$(SRC))

DFILES = $(subst .c,.d,$(SRC))


#########################################################
# explicite rules
#########################################################

all: $(BIN)

$(BIN): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LDFLAGS)

install:
	@echo install to do

uninstall:
	@echo uninstall to do

clean:
	$(RM) $(OBJ) $(OBJ_GTK) $(DFILES) $(DFILES_GTK)

distclean: clean
	$(RM) $(BIN) $(BIN_GTK)

mostlyclean:
	@echo mostlyclean to do

maintainer-clean:
	@echo maintainer-clean to do

TAGS:
	@echo TAGS to do

info:
	@echo info to do


#########################################################
# Implicite rules
#########################################################

.SUFFIXES : .c .o .d

%.o : %.c
	$(CC) $(CFLAGS) -c $(CPPFLAGS) $< -o $@

%.d : %.c
	$(SHELL) -ec '$(CC) -MM $(CPPFLAGS) $< \
        | sed '\''s/\($*\.o\)[ :]*/\1 $@ : /g'\'' > $@'

# include dependance files
-include $(DFILES)














